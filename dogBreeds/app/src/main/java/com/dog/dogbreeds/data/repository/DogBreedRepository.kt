package com.dog.dogbreeds.data.repository

import com.dog.dogbreeds.models.DogBreed
import com.dog.dogbreeds.models.DogBreedImage
import com.dog.dogbreeds.models.DogBreedModel
import io.reactivex.Observable

interface DogBreedRepository {
    fun getDogBreeds(): Observable<List<DogBreed>>
    fun getDogBreedImage(breed: String) : Observable<DogBreedImage>
}