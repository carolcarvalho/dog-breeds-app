package com.dog.dogbreeds.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dog.dogbreeds.models.DogBreed
import com.dog.dogbreeds.models.DogBreedImage

@Database(entities = [(DogBreed::class), (DogBreedImage::class)], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun dogBreedDAO(): DogBreedDao
    abstract fun dogBreedImageDAO(): DogBreedImageDAO
}