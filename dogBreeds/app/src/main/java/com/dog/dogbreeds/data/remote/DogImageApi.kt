package com.dog.dogbreeds.data.remote

import com.dog.dogbreeds.models.DogImageApiResponse
import io.reactivex.Single

interface DogImageApi {
    fun getDogImageByBree(breedName: String): Single<DogImageApiResponse>
}