package com.dog.dogbreeds.data.repository

import com.dog.dogbreeds.data.remote.DogBreedApi
import com.dog.dogbreeds.data.remote.DogImageApi
import com.dog.dogbreeds.models.DogBreed
import com.dog.dogbreeds.models.DogBreedImage
import com.dog.dogbreeds.room.DogBreedDao
import com.dog.dogbreeds.room.DogBreedImageDAO
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DogBreedRepositoryImpl @Inject constructor(
    val dogBreedApi: DogBreedApi,
    val dogImageApi: DogImageApi,
    val dogBreedDao: DogBreedDao,
    val dogBreedImageDAO: DogBreedImageDAO
) : DogBreedRepository {

    override fun getDogBreeds(): Observable<List<DogBreed>> {
        return dogBreedDao.getDogBreeds().switchIfEmpty(
            Maybe.fromSingle {
                getRemoteBreeds()
            }
        ).toObservable()
    }

    override fun getDogBreedImage(breed: String): Observable<DogBreedImage> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    private fun getRemoteBreeds() =
        dogBreedApi.getDogBreeds().doOnSuccess { dogBreed ->
            dogBreed.breedsName.forEach {
                updateDogBreedsDAO(it)
            }
        }.subscribeOn(Schedulers.single())


    private fun updateDogBreedsDAO(breed: String) {
        dogBreedDao.deleteDogBreeds()
        dogBreedDao.insertDogBreeds(DogBreed(name = breed))
    }

}