package com.dog.dogbreeds.data.remote

import com.dog.dogbreeds.models.DogBreedApiResponse
import com.dog.dogbreeds.models.DogBreedModel
import io.reactivex.Single

interface DogBreedApi {
    fun getDogBreeds() : Single<DogBreedApiResponse>
}