package com.dog.dogbreeds.models

data class DogBreedModel(val name: String)