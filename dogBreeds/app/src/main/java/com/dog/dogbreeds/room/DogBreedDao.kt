package com.dog.dogbreeds.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dog.dogbreeds.models.DogBreed
import io.reactivex.Maybe

@Dao
interface DogBreedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDogBreeds(breeds: DogBreed)

    @Query("SELECT * FROM DogBreed")
    fun getDogBreeds(): Maybe<List<DogBreed>>

    @Query("DELETE FROM DogBreed")
    fun deleteDogBreeds()
}