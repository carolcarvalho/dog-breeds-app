package com.dog.dogbreeds.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DogBreedImage(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val breedName: String,
    val url: String
)
