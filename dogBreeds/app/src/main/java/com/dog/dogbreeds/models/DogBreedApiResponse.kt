package com.dog.dogbreeds.models

import com.google.gson.annotations.SerializedName

data class DogBreedApiResponse(
    val status: String = "",
    @SerializedName("message")
    val breedsName: List<String> = listOf()
)