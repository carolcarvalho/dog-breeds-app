package com.dog.dogbreeds.di.module

import android.content.Context
import com.dog.dogbreeds.Application
import dagger.Module

@Module
class ApplicationModule {

    fun provideContext(app: Application): Context = app.applicationContext
}