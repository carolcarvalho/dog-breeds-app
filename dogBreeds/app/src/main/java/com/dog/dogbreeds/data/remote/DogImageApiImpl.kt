package com.dog.dogbreeds.data.remote

import com.dog.dogbreeds.models.DogImageApiResponse
import io.reactivex.Single
import javax.inject.Inject

class DogImageApiImpl @Inject constructor(
    private val remoteService: DogRemoteService
) : DogImageApi {
    override fun getDogImageByBree(breedName: String): Single<DogImageApiResponse> =
        remoteService.getDogBreedImage(breedName)
}