package com.dog.dogbreeds.data.remote

import com.dog.dogbreeds.models.DogBreedApiResponse
import io.reactivex.Single
import javax.inject.Inject

class DogBreedApiImpl @Inject constructor(
    private val remoteService: DogRemoteService
) : DogBreedApi {

    override fun getDogBreeds(): Single<DogBreedApiResponse> =
        remoteService.getBreeds()
}