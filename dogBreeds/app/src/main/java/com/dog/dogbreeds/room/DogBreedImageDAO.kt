package com.dog.dogbreeds.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dog.dogbreeds.models.DogBreedImage

@Dao
interface DogBreedImageDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDogBreedImage(dogBreedImage: DogBreedImage)

    @Query("SELECT url FROM DogBreedImage WHERE breedName = :breed COLLATE NOCASE LIMIT 1")
    fun getDobBreedUrlImage(breed: String): LiveData<DogBreedImage>

}