package com.dog.dogbreeds.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dog.dogbreeds.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dog_breeds)
    }
}
