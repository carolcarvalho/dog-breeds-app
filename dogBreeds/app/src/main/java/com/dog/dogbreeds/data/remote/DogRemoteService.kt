package com.dog.dogbreeds.data.remote

import com.dog.dogbreeds.models.DogBreedApiResponse
import com.dog.dogbreeds.models.DogImageApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface DogRemoteService {

    @GET("breeds/list")
    fun getBreeds(): Single<DogBreedApiResponse>

    @GET("breed/{breed}/images/random")
    fun getDogBreedImage(@Path("breed") breed: String): Single<DogImageApiResponse>
}