package com.dog.dogbreeds.models

import com.google.gson.annotations.SerializedName

data class DogImageApiResponse(
    @SerializedName("message")
    val url: String = "",
    val status: String = ""
)