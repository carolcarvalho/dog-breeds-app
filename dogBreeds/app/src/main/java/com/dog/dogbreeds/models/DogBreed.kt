package com.dog.dogbreeds.models


import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class DogBreed (
    @PrimaryKey(autoGenerate = true) val id: Long = 0,

    @NonNull
    val name: String
)